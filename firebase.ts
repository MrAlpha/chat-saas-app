import { getApp, getApps, initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
import { getFunctions } from "firebase/functions";

const firebaseConfig = {
  apiKey: "AIzaSyCARCVK2J9yyzgyfkzPITeYLpGtFiFanH8",
  authDomain: "chat-saas-app.firebaseapp.com",
  projectId: "chat-saas-app",
  storageBucket: "chat-saas-app.appspot.com",
  messagingSenderId: "396251429824",
  appId: "1:396251429824:web:5f1a6fdcc7c76f0c5c2625",
};

// Initialize Firebase
const app = getApps().length ? getApp() : initializeApp(firebaseConfig);
const db = getFirestore(app);
const auth = getAuth(app);
const functions = getFunctions(app);

export { db, auth, functions };
